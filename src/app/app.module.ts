import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { FirstFormComponent } from './first-form/first-form.component';
@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    FirstFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  exports: [
    FirstFormComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
