import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  landingForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
  }

  formValidation() {
    this.landingForm = this.formBuilder.group({
      product_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      quantity_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      faulty_1: new FormControl(
        '',
      ),
      faultyQuantity_1: new FormControl(
        '',
      ),
      packageType: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      importDate: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      // messageBy: new FormControl(
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //   ])
      // ),
      // dONumber: new FormControl(
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //   ])
      // ),
      vehicleNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      driverContactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
    });
  }

}
